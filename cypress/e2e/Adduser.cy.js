describe("Users", () => {
  it("Adding users", () => {
    //Visit website
    cy.visit("https://demo.aakashtel.com/home/dashboard");

    cy.get('input[type="email"]')
      .type("ramlal@gmail.com")
      .should("have.attr", "placeholder", "Enter your email");

    //password
    cy.get('input[type="Password"]')
      .type("password")
      .should("have.attr", "placeholder", "Enter your password");

    //Login
    cy.get('button[type="button"]').click();

    cy.contains("Staff").click();
    cy.contains("User").click();
    cy.contains("Add New User").click();

    //Forms:
    //Fullname
    cy.get('input[name="name"]').type("Ram");
    //User Id
    cy.get('input[name="username"]').type("101");
    //email
    cy.get('input[name="email"]').type("Ram@gmail.com");
    //Password
    cy.get('input[name="password"]').type("1234@Qwer");
    //Confirm Password
    cy.get('input[name="password_confirmation"]').type("1234@Qwer");
    //Phone number
    cy.get('input[name="mobile"]').type("9876768906");
    //Address
    cy.get('input[name="address"]').type("Nakxal");
    //Assign department
    cy.get("#react-select-2-input ").click(),
      cy.get('input[name="colors"]').select();
  });
});
