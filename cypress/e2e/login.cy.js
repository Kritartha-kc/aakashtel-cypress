describe("Login Testcase", () => {
  it("when entering user detail", () => {
    //Visit Website
    cy.visit("http://demo.aakashtel.com/login");

    //gmail
    cy.get('input[type="email"]')
      .type("ramlal@gmail.com")
      .should("have.attr", "placeholder", "Enter your email");

    //password
    cy.get('input[type="Password"]')
      .type("password")
      .should("have.attr", "placeholder", "Enter your password");

    //Login
    cy.get('button[type="button"]').click();
  });
});
