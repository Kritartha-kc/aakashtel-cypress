describe("Campaign  Testcase", () => {
  it("when entering user detail", () => {
    //Visit website
    cy.visit("https://demo.aakashtel.com/home/dashboard");

    cy.get('input[type="email"]')
      .type("ramlal@gmail.com")
      .should("have.attr", "placeholder", "Enter your email");

    //password
    cy.get('input[type="Password"]')
      .type("password")
      .should("have.attr", "placeholder", "Enter your password");

    //Login
    cy.get('button[type="button"]').click();

    cy.contains("Campaign").click();
    cy.contains("Create Campaign").click();

    //Campaign name:
    cy.get('input[placeholder="Enter campaign name"]').type("Test 01");
    // To numbers:
    cy.get('input[placeholder="Enter number..."]').type(
      "0000000000 {enter},0000000000 {enter} "
    );
    //Choose voice:
    cy.get("#demo-simple-select-helper").click(),
      cy.contains("welcome to akash tel").click();

    //Note:
    cy.get('input[placeholder="Enter note"]').type("Test 01");

    //Preview campaign:
    cy.contains("Preview").click();

    //Start campaign:
    cy.contains("Start campaign").click();
  });
});
