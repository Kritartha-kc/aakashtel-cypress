describe("Register Testcase", () => {
  it("when entering user detail", () => {
    //Registration page url
    cy.visit("https://demo.aakashtel.com/register");

    //Name:
    cy.get('input[placeholder="Enter your full name"]').type("Kritartha");

    //company name:
    cy.get('input[placeholder="Enter your company name"]').type("E-signature");

    //Mobile number:
    cy.get('input[placeholder="Enter your mobile number"]').type("9867676767");

    //Email:
    cy.get('input[placeholder="Enter your email"]').type(
      "kritartha1@gmail.com"
    );

    //Password Password should have at least one numerical digit(0-9). Password length should be 8 characters. Password should have at least one lowercase letter(a - z). Password should have at least one uppercase letter(A - Z).:
    cy.get('input[placeholder="Enter your password"]').type("1234@Qwer");

    // cy.get('input[placeholder="Enter your password"]').should(
    //   "contain",
    //   "Password should have at least one numerical digit(0-9)"
    // );

    //Confirm password:
    cy.get('input[placeholder="Confirm your password"]').type("1234@Qwer");

    //Terms and condition:
    cy.get('input[name="termCondition"]').click();

    //Register:
    cy.get('button[type="button"]').click();
  });
});
